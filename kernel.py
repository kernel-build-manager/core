import os, json

from . import tools

class Kernel():
    def __init__(self, 
                name: str,
                version: str,
                arch: str,
                target_android: str,
                stable_release: bool,
                device_codename: str,
                defconfig: str,
                auto_dtb: bool,
                clean_build: bool,
                kernel_config_dir: str,
                toolchain_dir: str,
                toolchain32_dir: str = None):
        self.name = name
        self.config_dir = kernel_config_dir
        self.source_code = os.path.join(self.config_dir, "source")
        self.version = version
        self.arch = arch
        self.target_android = target_android
        self.stable_release = stable_release
        self.device_codename = device_codename
        self.defconfig = defconfig
        self.auto_dtb = auto_dtb
        self.clean_build = clean_build
        self.toolchain = toolchain_dir

        if (self.arch == "arm64"):
            if not (toolchain32_dir):
                # Some arm64 kernels need the arm toolchain alongside the arm64 one.
                raise FileNotFoundError("ARM toolchain was not provided.")
            self.toolchain32 = toolchain32_dir

    def save_config(self):
        if not (os.path.exists(self.config_dir)):
            os.makedirs(self.config_dir)

        _config_file = os.path.join(self.config_dir, "config.json")
        _config = vars(self)
        with open(_config_file, 'w') as _output:
            json.dump(_config, _output, indent=4)

    def make_clean(self):
        _images = [
            f"{self.source_code}/arch/{self.arch}/boot/zImage",
            f"{self.source_code}/arch/{self.arch}/boot/Image.gz-dtb",
            f"{self.source_code}/arch/{self.arch}/boot/Image",
            f"{self.source_code}/arch/{self.arch}/boot/Image.gz"
        ]

        for _image in _images:
            if (os.path.exists(_image)):
                os.remove(_image)
        
        tools.run_command("make clean", self.source_code)

    def load_defconfig(self):
        tools.run_command(f"make {self.defconfig}",
                          self.source_code,
                          [f"ARCH={self.arch}"])

    def compile(self, thread_limit: int = None):

        # Declaration for necessary variables.
        _variables = [
            f"ARCH={self.arch}",
            f"SUB_ARCH={self.arch}",
            f"CROSS_COMPILE={self.toolchain}",
        ]

        # Add CROSS_COMPILE_ARM32 for some devices
        if (self.arch == "arm64"):
            _variables.append(f"CROSS_COMPILE_ARM32={self.toolchain32}")

        _threads = int(tools.run_command("nproc", 
                                         capture_output=True).stdout.strip())
        if (thread_limit) and (thread_limit <= _threads):
            _threads = thread_limit

        # Start compiling.
        tools.run_command(f"make -j{_threads}",
                          self.source_code,
                          _variables)
