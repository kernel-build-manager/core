import subprocess

def run_command(command: str,
                working_dir: str = None,
                exports: list() = None,
                capture_output: bool = False):
    _command = ""

    if exports:
        for var in exports:
            _command += "export {} && ".format(var)

    _command += command

    return subprocess.run(
        _command,
        cwd=working_dir,
        shell=True,
        capture_output = capture_output
    )
